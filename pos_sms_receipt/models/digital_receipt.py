# -*- coding: utf-8 -*-
#

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class DigitalReceipt(models.Model):
    _inherit = "pos.order"

    @api.multi
    def action_pos_order_paid(self):
        ret = super(DigitalReceipt, self).action_pos_order_paid()
        #send digital receipt
        self.send_sms_receipt()

        return ret

    @api.multi
    def send_sms_receipt(self):
        sms_template = self.env['api.at_sms.template'].search([('sms_type', '=', 'sms_digital_receipt')])
        if sms_template:
            if self.partner_id:
                partner = self.partner_id
                if partner.phone.startswith('0'):
                  MSISDN = partner.phone
                elif partner.phone.startswith('+254'):
                  MSISDN = partner.phone
                else:
                  raise UserError(_("Phone Number is not valid."))
                  print "No SMS found"
                order_lines = ["{}-{}".format(l.product_id.name, int(l.price_subtotal)) for l in self.lines]
                order_str = ",".join(order_lines)
                total = int(sum(self.lines.mapped('price_subtotal')))
                fname = self.partner_id.name.split(' ', 1)[0]
                msg = "Hi {},\n{}\nTotal: {}\n\nHuman Hair Centre.0701510370\nInstagram: humanhaircentre".format(fname, order_str, total)

                vals = {
                    'sms_to': MSISDN,
                    'sms_type': sms_template[0].sms_type,
                    'sms_msg': msg
                }
                sms = self.env['api.at_sms'].create(vals)

                sms.send_sms()
