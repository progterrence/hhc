﻿# -*- coding: utf-8 -*-
# © 2016 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'POS SMS Receipt',
    'version': '10.0.1.0.0',
    'summary': 'Salon',
    'author': 'Sunflower IT',
    'website': 'http://sunflowerweb.nl',
    'category': 'Enterprise Specific Management',
    'sequence': 0,
    'depends': [
        'at_api',
        'point_of_sale'
    ],
    'demo': [],
    'data': [
        'views/digital_receipt.xml',
    ],
    'application': True,
    'installable': True
}
