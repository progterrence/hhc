# -*- coding: utf-8 -*-
#

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class ApiAuth(models.Model):
    _name = "api.at_auth"

    username = fields.Char(string='Username', required=True)
    apikey = fields.Char(string='Api Key', required=True)

