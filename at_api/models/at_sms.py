# -*- coding: utf-8 -*-
#

from odoo import models, fields, api, _
from ..gateway.AfricasTalkingGateway import AfricasTalkingGateway, AfricasTalkingGatewayException

SMS_TYPE = [
    ("sms_digital_receipt", "Digital Receipt")
]


class SMSApiTemplate(models.Model):
    _name = "api.at_sms.template"

    sms_type = fields.Selection(SMS_TYPE, default='sms_digital_receipt')
    sms_msg = fields.Text(string='SMS Message', required=True)


class ApiSms(models.Model):
    _name = "api.at_sms"

    sms_name = fields.Char(string='Name')
    sms_to = fields.Text(string='Recipient')
    sms_type = fields.Selection(SMS_TYPE, string='SMS Type', required=True)
    sms_msg = fields.Text(string='SMS Message', required=True)

    @api.multi
    def _get_gateway(self):
        api = self.env['api.at_auth'].search([])[0]
        username = api.username
        apiKey = api.apikey
        gateway = AfricasTalkingGateway(username, apiKey)
        return gateway

    @api.multi
    def send_sms(self):
        gateway = self._get_gateway()
        gateway.sendMessage(self.sms_to, self.sms_msg)