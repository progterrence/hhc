﻿# -*- coding: utf-8 -*-
# © 2016 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Africas Talking APIs',
    'version': '10.0.1.0.0',
    'summary': 'APIs',
    'author': 'Sunflower IT',
    'website': 'http://sunflowerweb.nl',
    'category': 'Enterprise Specific Management',
    'sequence': 0,
    'depends': [
        # 'primapesa_base',
    ],
    'demo': [],
    'data': [
        'security/ir.model.access.csv',
        'views/at_api.xml',
        'views/at_sms.xml',
        'data/sms.xml',
    ],
    # 'external_dependencies': {
    #     'python': ['AfricastalkingGateway'],
    # },
    'application': True,
    'installable': True
}
